# bootstrapping a litelement project

## Start

### Start a dev server

```shell
polymer serve
```

### Build for production and serve locally

Build your project and serve the build locally:

```
polymer build
polymer serve build/default
```

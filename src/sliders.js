import { LitElement, html } from 'lit-element';

class SliderControl extends LitElement {
    static get properties() {
        return {
          minValue: { type: Number },
          maxValue: { type: Number },
          initialValue: { type: Number },
          value: {type: Number },
          step: { type: Number },
          event: { type: String }
        };
      }
    
      firstUpdated(changedProperties) {
        this.value = this.initialValue;
      }

      changeValue(e) {
        this.dispatchEvent(
          new CustomEvent(this.event, {  
            bubbles: true,
            composed: true,
            detail: {
              value: e.target.value
            } 
          })
        );
      }

      render() {
        return html`
        <input type="range" min="${this.minValue}" max="${this.maxValue}" step="${this.step}" value="${this.value}" @input="${this.changeValue}">
        `;
      }
}
customElements.define('slider-control', SliderControl);
import { LitElement, html, css } from 'lit-element';

import { externalStyling } from './external-styling';
import { appStyling } from './app-styling';

export class MainApplication extends LitElement {

  constructor() {
    super()

    /* synthData is intended to be the 'single source of truth' for all settings.
    This construction is probably not strictly necessary as the main app
    would able to access control values directly using something like:
    this.shadowRoot.getElementById('volume').value
    However, keeping everything in a central structure seems at this point to
    be the right thing to do. */
    this.synthData = {
      curve: 1,
      cutoff: 0.2,
      depth: 0.3,
      rate1: 0.4,
      rate2: 0.5,
      attack: 0.5,
      decay: 0.4,
      sustain: 0.3,
      release: 0.2,
      volume: 0.1,
      mod: 1,
      follow: 1,
      velocity: 1,
      midi: 0,
      test: 0
    }

    this.addEventListener('play-test-tone', e => {
      if (e.detail.value === 1) {
        this.prepareAudioContext();
        this.prepareOsc();
        this.oscillator.frequency.setValueAtTime(440, audioContext.currentTime);
        this.oscillator.start();
      }
      if (e.detail.value === 0) {
        this.oscillator.stop();
      }
    }, false);

    this.addEventListener('volume', e => {
      this.synthData['volume'] = e.detail.value;
      if (typeof(this.gain) != 'undefined') {
        this.gain.gain.value = parseFloat(this.synthData['volume']);
      }
    }, false);

    this.addEventListener('curve', e => {
      this.synthData['curve'] = e.detail.value;
    }, false);
  }

  prepareAudioContext() {
    if (typeof(window.audioContext) === "undefined") {
      window.audioContext = new AudioContext();
    }
    if (typeof(this.gain) === "undefined") {
      this.gain = audioContext.createGain();
      this.gain.gain.value = parseFloat(this.synthData['volume']);
    }
  }

  prepareOsc () {
    // The oscillator can only be used once, so we must create a new once for each note!
    this.oscillator = audioContext.createOscillator();
    this.oscillator.type = 'square';
    this.oscillator.connect(this.gain).connect(audioContext.destination);
  }

  static get styles() {
    return [
      externalStyling,
      appStyling,
    ];
  }

  render() {
    return html`
      <main role="main" class="container">
        <div class="row">
          <div class="column synth-cell">
            <div class="synth-label">CURVE</div>
            <div class="synth-control"><slider-control minValue="0" maxValue = "3" initialValue=${this.synthData['curve']} step="1" event="curve"></slider-control></div>
          </div>
          <div class="column synth-cell">
              <div class="synth-label">CUTOFF</div>
              <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['cutoff']} step="0.01" event="cutoff"></slider-control></div>
          </div>
          <div class="column synth-cell">
            <div class="synth-label">DEPTH</div>
            <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['depth']} step="0.01" event="depth"></slider-control></div>
          </div>
          <div class="column synth-cell">
              <div class="synth-label">RATE 1</div>
              <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['rate1']} step="0.01" event="rate1"></slider-control></div>
          </div>
          <div class="column synth-cell">
              <div class="synth-label">RATE 2</div>
              <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['rate2']} step="0.01" event="rate2"></slider-control></div>
          </div>
        </div>
        <div class="row">
            <div class="column synth-cell">
                <div class="synth-label">ATTACK</div>
                <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['attack']} step="0.01" event="attack"></slider-control></div>
            </div>
            <div class="column synth-cell">
                <div class="synth-label">DECAY</div>
                <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['decay']} step="0.01" event="decay"></slider-control></div>
            </div>
            <div class="column synth-cell">
                <div class="synth-label">SUSTAIN</div>
                <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['sustain']} step="0.01" event="sustain"></slider-control></div>
            </div>
            <div class="column synth-cell">
              <div class="synth-label">RELEASE</div>
              <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['release']} step="0.01" event="release"></slider-control></div>
            </div>
            <div class="column synth-cell">
                <div class="synth-label">VOLUME</div>
                <div class="synth-control"><slider-control minValue="0" maxValue = "1" initialValue=${this.synthData['volume']} step="0.01" event="volume"></slider-control></div>
            </div>
          </div>
          <div class="row synth-row">
            <div class="column synth-cell">
                <div class="synth-label">MOD</div>
                <div class="synth-control"><stateful-button initialValue="1" maxValue="2" event=${this.synthData['mod']}></audio-aware-button></div>
            </div>
            <div class="column synth-cell">
                <div class="synth-label">FOLLOW</div>
                <div class="synth-control"><stateful-button initialValue="1" maxValue="1" event=${this.synthData['follow']}></audio-aware-button></div>
            </div>
            <div class="column synth-cell">
                <div class="synth-label">VELOCITY</div>
                <div class="synth-control"><stateful-button initialValue="1" maxValue="1" event=${this.synthData['velocity']}></audio-aware-button></div>
            </div>
            <div class="column synth-cell">
              <div class="synth-label">MIDI</div>
              <div class="synth-control"><stateful-button initialValue="0" maxValue="1" event=${this.synthData['midi']}></audio-aware-button></div>
            </div> 
          <div class="column synth-cell">
              <div class="column synth-cell">
                  <div class="synth-label">TEST</div>
                  <div class="synth-control"><stateful-button initialValue=${this.synthData['test']} maxValue="1" event="play-test-tone"></audio-aware-button></div>
              </div>
            </div>
          </div> 
      </main>
    `
  }
}

customElements.define('main-application', MainApplication);

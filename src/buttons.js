import { LitElement, html } from 'lit-element';

class StatefulButton extends LitElement {

  static get properties() {
    return { 
      value: { type: Number },
      maxValue: { type: Number },
      initialValue: { type: Number },
      event: { type: String }
    };
  }

  firstUpdated(changedProperties) {
    this.value = 0;
  }

  setNextState() {
    if (this.value >= this.maxValue) {
      this.value = 0
    }
    else {
      this.value++;
    }
    this.dispatchEvent(
      new CustomEvent(this.event, {  
        bubbles: true,
        composed: true,
        detail: {
          value: this.value
        } 
      })
    );
    this.requestUpdate();
  }

  getState() {
    return this.value;
  }

  getColor() {
    switch(this.value) {
      case 0:
        return "#303030";
      case 1:
        return "blue";
      case 2:
        return "green";
      case 3:
        return "yellow";
      case 4:
        return "red";
      default:
        return "magenta";
    }
  }

  render() {
    return html`
    <button @click="${this.setNextState}" style="background-color:${this.getColor()};min-width:35px;min-height:20px"></button>
    `;
  }
}
customElements.define('stateful-button', StatefulButton);

